package com.example.shutterstockdemo.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper {

	private static final String DATABASE_NAME = "shutterstock.db";
	public static final String TABLE_NAME = "media_library";
	public static final String COLUMN_ID = "_id";
	public static final String COLUMN_MEDIA_TYPE = "media_type";
	public static final String COLUMN_URL = "url";
	public static final String COLUMN_FILE_LOCATION = "file_location";
	public static final String COLUMN_TOTAL_BYTES = "total_bytes";
	public static final String COLUMN_DOWNLOADED_BYTES = "downloaded_bytes";
	public static final String COLUMN_ARTIST_NAME = "artist_name";
	public static final String COLUMN_THUMBNAIL_URL = "thumbnail_url";
	public static final String COLUMN_DOWNLOAD_STATUS = "download_status";
	public static final String[] ALL_COLUMNS = {COLUMN_ID, COLUMN_MEDIA_TYPE, COLUMN_URL, COLUMN_FILE_LOCATION, COLUMN_TOTAL_BYTES, COLUMN_DOWNLOADED_BYTES, COLUMN_ARTIST_NAME, COLUMN_THUMBNAIL_URL, COLUMN_DOWNLOAD_STATUS};
	
	
	private static final int DATABASE_VERSION = 1;
	
	private static final String CREATE_SCHEMA = String.format("" 
		+ "create table %s (" 
		+ "%s integer primary key autoincrement, "
		+ "%s varchar not null, "
		+ "%s varchar not null, "
		+ "%s varchar, "
		+ "%s integer, "
		+ "%s integer, "
		+ "%s varchar not null, "
		+ "%s varchar,"
		+ "%s varchar);",
		TABLE_NAME, COLUMN_ID, COLUMN_MEDIA_TYPE, COLUMN_URL, COLUMN_FILE_LOCATION, COLUMN_TOTAL_BYTES, COLUMN_DOWNLOADED_BYTES, COLUMN_ARTIST_NAME, COLUMN_THUMBNAIL_URL, COLUMN_DOWNLOAD_STATUS);
	
	private static final String UPGRADE_SCHEMA = "drop table if exists " + TABLE_NAME;
	
	public DatabaseHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}
	
	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(CREATE_SCHEMA);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL(UPGRADE_SCHEMA);
		onCreate(db);
	}

}
