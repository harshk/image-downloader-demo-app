package com.example.shutterstockdemo.db;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.shutterstockdemo.model.internal.MediaItem;
import com.example.shutterstockdemo.utils.Utils.MediaType;

public class MediaItemDAO {
	
	public static final String TAG = MediaItemDAO.class.getSimpleName();

	private SQLiteDatabase database;
	private DatabaseHelper dbHelper;
	
	public static enum DownloadStatusEnum {INCOMPLETE, COMPLETE, STOPPED};
	
	public MediaItemDAO(Context context) {
		dbHelper = new DatabaseHelper(context);
	}
	
	public void open() throws SQLException {
		if (database == null || !database.isOpen()) {
			database = dbHelper.getWritableDatabase();
		}
	}
	
	public void close() {
		//dbHelper.close();
	}

	public Long addMediaItemRow(MediaItem item) {
		Log.d(TAG, "HK-DB addMediaItemRow");
		ContentValues values = new ContentValues();
		values.put(DatabaseHelper.COLUMN_MEDIA_TYPE, item.getMediaType().name());
		values.put(DatabaseHelper.COLUMN_URL, item.getUrl());
		values.put(DatabaseHelper.COLUMN_FILE_LOCATION, item.getFileLocation());
		values.put(DatabaseHelper.COLUMN_TOTAL_BYTES, item.getTotalBytes());
		values.put(DatabaseHelper.COLUMN_DOWNLOADED_BYTES, item.getDownloadedBytes());
		values.put(DatabaseHelper.COLUMN_ARTIST_NAME, item.getArtistName());
		values.put(DatabaseHelper.COLUMN_THUMBNAIL_URL, item.getThumbnailUrl());
		values.put(DatabaseHelper.COLUMN_DOWNLOAD_STATUS, item.getDownloadStatus());
		
		
		Long insertId = database.insert(DatabaseHelper.TABLE_NAME, null, values);
		return insertId;
	}
	
	public MediaItem getMediaItemRow(Long id) {
		Log.d(TAG, "HK-DB getMediaItemRow");
		String[] args = {id.toString()};
		Cursor cursor = database.query(DatabaseHelper.TABLE_NAME, DatabaseHelper.ALL_COLUMNS, DatabaseHelper.COLUMN_ID + "=?", args, null, null, null, null);
		cursor.moveToFirst();
		MediaItem item = createMediaItem(cursor);
		cursor.close();
		Log.d(TAG, "getMediaItemRow() [1.2] : item.toString() = " + item);
		return item;
	}
	
	public MediaItem getMediaItemRowByUrl(String url) {
		Log.d(TAG, "HK-DB getMediaItemRowByUrl");
		String[] args = {url};
		Cursor cursor = database.query(DatabaseHelper.TABLE_NAME, DatabaseHelper.ALL_COLUMNS, DatabaseHelper.COLUMN_URL + "=?", args, null, null, null, null);
		if (cursor == null || cursor.getCount() == 0) {
			return null;
		}
		cursor.moveToFirst();
		MediaItem item = createMediaItem(cursor);
		cursor.close();
		return item;
	}
	
	public void deleteMediaItemRow(Long id) {
		Log.d(TAG, "HK-DB deleteMediaItemRow");
		String[] args = {id.toString()};
		database.delete(DatabaseHelper.TABLE_NAME, DatabaseHelper.COLUMN_ID + "=?", args);
	}
	
	public List<MediaItem> getAllMediaItemRows() {
		Log.d(TAG, "HK-DB getAllMediaItemRows");
		List<MediaItem> items = new ArrayList<MediaItem>();
		Cursor cursor = database.query(DatabaseHelper.TABLE_NAME, DatabaseHelper.ALL_COLUMNS, null, null, null, null, null);
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			items.add(createMediaItem(cursor));
			cursor.moveToNext();
		}
		cursor.close();
		return items;
	}
	
	public List<MediaItem> getCurrentlyDownloadingMediaItemRows() {
		Log.d(TAG, "HK-DB getCurrentlyDownloadingMediaItemRows");
		String selection = "total_bytes > 0 and (total_bytes != downloaded_bytes)";
		
		List<MediaItem> items = new ArrayList<MediaItem>();
		Cursor cursor = database.query(DatabaseHelper.TABLE_NAME, DatabaseHelper.ALL_COLUMNS, selection, null, null, null, null);
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			items.add(createMediaItem(cursor));
			cursor.moveToNext();
		}
		cursor.close();
		return items;
	}
	
	public List<MediaItem> getUnfinishedDownloads() {
		Log.d(TAG, "HK-DB getUnfinishedDownloads");
		String whereClause = "download_status = 'INCOMPLETE' and downloaded_bytes > 0";
		
		List<MediaItem> items = new ArrayList<MediaItem>();
		Cursor cursor = database.query(DatabaseHelper.TABLE_NAME, DatabaseHelper.ALL_COLUMNS, whereClause, null, null, null, null);
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			items.add(createMediaItem(cursor));
			cursor.moveToNext();
		}
		cursor.close();
		return items;
	}
	
	public void updateColumnValueDownloadedBytes(Long id, Integer downloadedBytes, String fileLocation) {
		Log.d(TAG, "HK-DB updateColumnValueDownloadedBytes");
		
		ContentValues values = new ContentValues();
		values.put(DatabaseHelper.COLUMN_DOWNLOADED_BYTES, downloadedBytes);
		database.update(DatabaseHelper.TABLE_NAME, values, DatabaseHelper.COLUMN_ID + "=" + id, null);
	}
	
	public void updateColumnValueDownloadedBytesAndStatus(Long id, Integer downloadedBytes, String fileLocation, DownloadStatusEnum newStatus) {
		Log.d(TAG, "HK-DB updateColumnValueDownloadedBytesAndStatus");
		
		ContentValues values = new ContentValues();
		values.put(DatabaseHelper.COLUMN_DOWNLOADED_BYTES, downloadedBytes);
		values.put(DatabaseHelper.COLUMN_DOWNLOAD_STATUS, newStatus.toString());
		database.update(DatabaseHelper.TABLE_NAME, values, DatabaseHelper.COLUMN_ID + "=" + id, null);
	}
	
	public void updateColumnValueTotalBytes(Long id, Integer totalBytes) {
		Log.d(TAG, "HK-DB updateColumnValueTotalBytes");
		
		ContentValues values = new ContentValues();
		values.put(DatabaseHelper.COLUMN_TOTAL_BYTES, totalBytes);
		database.update(DatabaseHelper.TABLE_NAME, values, DatabaseHelper.COLUMN_ID + "=" + id, null);
	}
	
	public void updateColumnFileLocation(Long id, String fileLocation) {
		Log.d(TAG, "HK-DB updateColumnFileLocation");
		
		ContentValues values = new ContentValues();
		values.put(DatabaseHelper.COLUMN_FILE_LOCATION, fileLocation);
		database.update(DatabaseHelper.TABLE_NAME, values, DatabaseHelper.COLUMN_ID + "=" + id, null);
	}
	
	public void updateColumnValueDownloadStatus(Long id, DownloadStatusEnum newStatus) {
		Log.d(TAG, "HK-DB updateColumnValueDownloadStatus");
		
		if (newStatus.toString().equals(DownloadStatusEnum.STOPPED.toString())) {
			Cursor c = database.rawQuery("UPDATE media_library SET download_status = 'STOPPED' WHERE _id=" + id, null);
			c.moveToFirst();
		}
		
		ContentValues values = new ContentValues();
		values.put(DatabaseHelper.COLUMN_DOWNLOAD_STATUS, newStatus.toString());
		database.update(DatabaseHelper.TABLE_NAME, values, DatabaseHelper.COLUMN_ID + "=" + id, null);
	}
	
	public MediaItem createMediaItem(Cursor cursor) {
		MediaItem item = new MediaItem();
		item.setId(cursor.getInt(cursor.getColumnIndex(DatabaseHelper.COLUMN_ID)));
		item.setMediaType(MediaType.valueOf(cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_MEDIA_TYPE))));
		item.setUrl(cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_URL)));
		item.setFileLocation(cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_FILE_LOCATION)));
		item.setTotalBytes(cursor.getInt(cursor.getColumnIndex(DatabaseHelper.COLUMN_TOTAL_BYTES)));
		item.setDownloadedBytes(cursor.getInt(cursor.getColumnIndex(DatabaseHelper.COLUMN_DOWNLOADED_BYTES)));
		item.setArtistName(cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_ARTIST_NAME)));
		item.setThumbnailUrl(cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_THUMBNAIL_URL)));
		item.setDownloadStatus(cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_DOWNLOAD_STATUS)));
		
		return item;
	}
	
}
