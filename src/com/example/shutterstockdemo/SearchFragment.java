package com.example.shutterstockdemo;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.shutterstockdemo.db.MediaItemDAO;
import com.example.shutterstockdemo.model.flickr.Photo;
import com.example.shutterstockdemo.model.flickr.PhotosWrapper;
import com.example.shutterstockdemo.model.internal.MediaItem;
import com.example.shutterstockdemo.utils.Utils.MediaType;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

public class SearchFragment extends Fragment {
	
	//TODO: add paging

	public static final String TAG = SearchFragment.class.getSimpleName();
	private static final String FLICKR_SEARCH_URL_FORMAT = "http://api.flickr.com/services/rest/?api_key=70b17e9a3b8add6e7e18052df5fb8574&method=flickr.photos.search&format=json&nojsoncallback=1&privacy_filter=1&extras=url_o,url_sq&per_page=100&text=%s"; 
	
	private ProgressBar progressBar;
	private EditText searchEditText;
	private Button searchButton;
	private TextView instructions;
	private GridView gridView;
	private FlickrSearchResultsAdapter adapter;
	private List<Photo> items = new ArrayList<Photo>();;
	
	private RequestQueue requestQueue = null;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		
		Log.d(TAG, "onCreateView() called!!");
		
        if (container == null) {
            return null;
        }
        
        requestQueue = Volley.newRequestQueue(getActivity());
        
        LinearLayout layout = (LinearLayout)inflater.inflate(R.layout.tab1, container, false);
        gridView = (GridView)layout.findViewById(R.id.search_results_listview);
        adapter = new FlickrSearchResultsAdapter(getActivity(), R.layout.flickr_search_item, items);
        gridView.setAdapter(adapter);
        gridView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				Photo item = adapter.getItem(position);
				MediaItemDAO dao = ShutterStockDemoApp.getMediaItemDao();
				
				if (dao.getMediaItemRowByUrl(item.getOriginalUrl()) != null) {
					Toast.makeText(getActivity().getApplicationContext(), "this image is already in your library!", Toast.LENGTH_SHORT).show();
					return;
				} else {
					Toast.makeText(getActivity().getApplicationContext(), "added this image to your library", Toast.LENGTH_SHORT).show();
				}
				
				MediaItem mi = new MediaItem();
				mi.setArtistName(item.getOwner());
				mi.setMediaType(MediaType.PICTURE);
				mi.setThumbnailUrl(item.getSquareThumbnailUrl());
				mi.setUrl(item.getOriginalUrl());
				mi.setTotalBytes(0);
				
				//add it to the library/db
				dao.addMediaItemRow(mi);
				
			}
		});
        
        progressBar = (ProgressBar)layout.findViewById(R.id.search_progress_bar);
        searchEditText = (EditText)layout.findViewById(R.id.search_box);
        searchButton = (Button)layout.findViewById(R.id.search_btn);
        instructions = (TextView)layout.findViewById(R.id.instructions);
        searchButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				
				//hide the keyboard
				InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(searchEditText.getWindowToken(), 0);
				
				if (StringUtils.isBlank(searchEditText.getText().toString())) {
					Toast.makeText(getActivity().getApplicationContext(), "please enter a search term", Toast.LENGTH_SHORT).show();
					return;
				}
				
				searchButton.setEnabled(false);
				
				String searchUrl = null;
				try {
					searchUrl = String.format(FLICKR_SEARCH_URL_FORMAT, URLEncoder.encode(searchEditText.getText().toString(), "UTF-8"));
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				}
				
				sendFlickrSearchRequest(searchUrl);
			}
		});
        return layout;
    }
	
	@Override
	public void onStart() {
		super.onStart();
		Log.d(TAG, "onStart() called!");
	}
	
	@Override
	public void onStop() {
		super.onStop();
		Log.d(TAG, "onStop() called!");
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		Log.d(TAG, "onDestroy() called!");
	}
	
	
	private void sendFlickrSearchRequest(String urlEncodedUrl) {
		Log.d(TAG, "urlEncodedUrl = " + urlEncodedUrl);
		Toast.makeText(getActivity().getApplicationContext(), urlEncodedUrl, Toast.LENGTH_SHORT).show();
		
		JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.GET, urlEncodedUrl, null, new Response.Listener<JSONObject>() {
			@Override
			public void onResponse(JSONObject response) {
				Log.d(TAG, "response = " + response.toString());
				progressBar.setVisibility(View.GONE);
				
				Gson g = new Gson();
				PhotosWrapper wrapper = g.fromJson(response.toString(), PhotosWrapper.class);
				List<Photo> items = wrapper.getPhotos().getPhotos();
				List<Photo> trimmedItems = new ArrayList<Photo>();
				for (Photo p : items) {
					if (StringUtils.isNotEmpty(p.getOriginalUrl())) {
						trimmedItems.add(p);
					}
				}
				adapter.clear();
				
				//adapter.addAll() was introduced in api 11, so we have to call adapter.add() instead for each item
				for (Photo p : trimmedItems) {
					adapter.add(p);
				}
				if (trimmedItems.size() > 0) {
					instructions.setVisibility(View.VISIBLE);
				}
				adapter.notifyDataSetChanged();
				
				Log.d(TAG, "gson resultCount = " + items.size());
				if (trimmedItems.size() == 0) {
					Toast.makeText(getActivity().getApplicationContext(), "0 results", Toast.LENGTH_LONG).show();
				}
				
				searchButton.setEnabled(true);
			}
		}, new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError error) {
				Toast.makeText(getActivity().getApplicationContext(), "Error occurred.  Please try again.", Toast.LENGTH_LONG).show();
				
				progressBar.setVisibility(View.GONE);
				searchButton.setEnabled(true);
			}
		});
		
		progressBar.setVisibility(View.VISIBLE);
		requestQueue.add(jsObjRequest);
	}
	
	
	
	public class FlickrSearchResultsAdapter extends ArrayAdapter<Photo> {

		List<Photo> items;
		Context context;
		int layoutResourceId;
		LayoutInflater inflater;
		
		public FlickrSearchResultsAdapter(Context context, int layoutResourceId, List<Photo> items) {
			super(context, layoutResourceId, items);
			this.items = items;
			this.context = context;
			this.layoutResourceId = layoutResourceId;
			inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		}
		
		@Override
		  public View getView(int position, View convertView, ViewGroup parent) {
			Photo item = getItem(position);
			
			ViewHolder holder;
			if (convertView == null)  {
	        	convertView = inflater.inflate(layoutResourceId, null);
	            holder = new ViewHolder();
	            
	            holder.title = (TextView)convertView.findViewById(R.id.title);
	            holder.thumbnail = (ImageView)convertView.findViewById(R.id.thumbnail);
	            
	            convertView.setTag(holder);
	        } else {
	        	holder = (ViewHolder)convertView.getTag();
	        }

			holder.title.setText(item.getTitle());
			//holder.artwork
			Picasso.with(context).load(item.getSquareThumbnailUrl()).into(holder.thumbnail);
			
			return convertView;
		}
		
		private class ViewHolder {
			TextView title;
			ImageView thumbnail;
		}
		
	}
	
}