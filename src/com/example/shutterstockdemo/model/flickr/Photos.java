package com.example.shutterstockdemo.model.flickr;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class Photos {

	@SerializedName("page")
	Integer page;
	
	@SerializedName("pages")
	Integer pages;
	
	@SerializedName("perpage")
	Integer perPage;
	
	@SerializedName("total")
	Integer total;
	
	@SerializedName("photo")
	List<Photo> photos;

	
	public Integer getPage() {
		return page;
	}

	public void setPage(Integer page) {
		this.page = page;
	}

	public Integer getPages() {
		return pages;
	}

	public void setPages(Integer pages) {
		this.pages = pages;
	}

	public Integer getPerPage() {
		return perPage;
	}

	public void setPerPage(Integer perPage) {
		this.perPage = perPage;
	}

	public Integer getTotal() {
		return total;
	}

	public void setTotal(Integer total) {
		this.total = total;
	}

	public List<Photo> getPhotos() {
		return photos;
	}

	public void setPhotos(List<Photo> photos) {
		this.photos = photos;
	}

	@Override
	public String toString() {
		return "PhotosWrapper [page=" + page + ", pages=" + pages
				+ ", perPage=" + perPage + ", total=" + total + ", photos="
				+ photos + "]";
	}
	
}
