package com.example.shutterstockdemo.model.flickr;

import com.google.gson.annotations.SerializedName;

public class Photo {

	@SerializedName("id")
	private Long id;
	
	@SerializedName("owner")
	private String owner;
	
	@SerializedName("secret")
	private String secret;
	
	@SerializedName("server")
	private Integer server;
	
	@SerializedName("farm")
	private Integer farm;
	
	@SerializedName("title")
	private String title;
	
	@SerializedName("ispublic")
	private Integer isPublic;
	
	@SerializedName("url_sq")
	private String squareThumbnailUrl;
	
	@SerializedName("url_o")
	private String originalUrl;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public String getSecret() {
		return secret;
	}

	public void setSecret(String secret) {
		this.secret = secret;
	}

	public Integer getServer() {
		return server;
	}

	public void setServer(Integer server) {
		this.server = server;
	}

	public Integer getFarm() {
		return farm;
	}

	public void setFarm(Integer farm) {
		this.farm = farm;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Integer getIsPublic() {
		return isPublic;
	}

	public void setIsPublic(Integer isPublic) {
		this.isPublic = isPublic;
	}

	public String getSquareThumbnailUrl() {
		return squareThumbnailUrl;
	}

	public void setSquareThumbnailUrl(String squareThumbnailUrl) {
		this.squareThumbnailUrl = squareThumbnailUrl;
	}

	public String getOriginalUrl() {
		return originalUrl;
	}

	public void setOriginalUrl(String originalUrl) {
		this.originalUrl = originalUrl;
	}

}
