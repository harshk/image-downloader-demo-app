package com.example.shutterstockdemo.model.flickr;

import com.google.gson.annotations.SerializedName;

public class PhotosWrapper {

	@SerializedName("photos")
	Photos photos;

	public Photos getPhotos() {
		return photos;
	}

	public void setPhotos(Photos photos) {
		this.photos = photos;
	}

	@Override
	public String toString() {
		return "PhotosWrapper [photos=" + photos + "]";
	}
	
}
