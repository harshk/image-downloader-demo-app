package com.example.shutterstockdemo.model.internal;

import java.io.Serializable;

import com.example.shutterstockdemo.utils.Utils.MediaType;

public class MediaItem implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2012323503968259289L;
	private Integer id;
	private MediaType mediaType;
	private String url;
	private String fileLocation; 
	private Integer totalBytes;
	private Integer downloadedBytes;
	private String artistName;
	private String thumbnailUrl;
	private String downloadStatus;
	
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public MediaType getMediaType() {
		return mediaType;
	}
	
	public void setMediaType(MediaType mediaType) {
		this.mediaType = mediaType;
	}
	
	public String getUrl() {
		return url;
	}
	
	public void setUrl(String url) {
		this.url = url;
	}
	
	public String getFileLocation() {
		return fileLocation;
	}
	
	public void setFileLocation(String fileLocation) {
		this.fileLocation = fileLocation;
	}
	
	public Integer getTotalBytes() {
		return totalBytes;
	}
	
	public void setTotalBytes(Integer totalBytes) {
		this.totalBytes = totalBytes;
	}
	
	public Integer getDownloadedBytes() {
		return downloadedBytes;
	}
	
	public void setDownloadedBytes(Integer downloadedBytes) {
		this.downloadedBytes = downloadedBytes;
	}
	
	
	public String getArtistName() {
		return artistName;
	}

	public void setArtistName(String artistName) {
		this.artistName = artistName;
	}

	public String getThumbnailUrl() {
		return thumbnailUrl;
	}

	public void setThumbnailUrl(String thumbnailUrl) {
		this.thumbnailUrl = thumbnailUrl;
	}
	
	public String getDownloadStatus() {
		return downloadStatus;
	}

	public void setDownloadStatus(String downloadStatus) {
		this.downloadStatus = downloadStatus;
	}

	public String toString() {
		StringBuffer b = new StringBuffer();
		b.append("id = " + id + "\n");
		b.append("mediaType = " + mediaType.name() + "\n");
		b.append("url = " + url + "\n");
		b.append("fileLocation = " + fileLocation + "\n");
		b.append("totalBytes = " + totalBytes + "\n");
		b.append("downloadedBytes = " + downloadedBytes + "\n");
		b.append("artistName = " + artistName + "\n");
		b.append("thumbnailUrl = " + thumbnailUrl + "\n");
		b.append("downloadStatus = " + downloadStatus + "\n");
		return b.toString();
	}
}