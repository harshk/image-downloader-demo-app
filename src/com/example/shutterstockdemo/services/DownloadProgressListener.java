package com.example.shutterstockdemo.services;

import com.example.shutterstockdemo.model.internal.MediaItem;

public interface DownloadProgressListener {

	public void progressUpdated(MediaItem mediaItem);
	
}
