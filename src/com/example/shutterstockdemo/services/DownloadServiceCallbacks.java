package com.example.shutterstockdemo.services;

import com.example.shutterstockdemo.model.internal.MediaItem;

public interface DownloadServiceCallbacks {

	public void receiveDownloadStatusUpdateFromThread(MediaItem item);
	public void removeFromAllDownloaders(MediaItem mediaItem);
}
