package com.example.shutterstockdemo.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.example.shutterstockdemo.utils.Utils;

public class ConnectivityChangeReceiver extends BroadcastReceiver {

	public static String TAG = ConnectivityChangeReceiver.class.getSimpleName();
	
	@Override
	public void onReceive(Context context, Intent intent) {
		Log.d(TAG, "onReceive() 1.0");
		if (Utils.isNetworkUp()) {
			Log.d(TAG, "onReceive() : The network is up, so start the DownloadService to see if any unfinished downloads need to be resumed.");
			Intent serviceIntent = new Intent(context, DownloadService.class);
			context.startService(serviceIntent);
		} else {
			Log.d(TAG, "onReceive() : The network is down.");
		}
	}

}
