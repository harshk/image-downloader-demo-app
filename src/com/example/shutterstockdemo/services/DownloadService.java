package com.example.shutterstockdemo.services;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

import com.example.shutterstockdemo.ShutterStockDemoApp;
import com.example.shutterstockdemo.db.MediaItemDAO;
import com.example.shutterstockdemo.model.internal.MediaItem;

public class DownloadService extends Service implements DownloadServiceCallbacks {

	public static final String TAG = DownloadService.class.getSimpleName();
	public static String INTENT_MEDIA_ITEM = "INTENT_MEDIA_ITEM_1";
	
	//if the onStartCommand() is executed more than once, this boolean variable
	//prevents restartUnfinishedDownloads() from being called multiple times
	private Boolean hasServiceEverStarted = false;
	
	private ExecutorService downloadExecutorService = Executors.newFixedThreadPool(5);
	private final IBinder mBinder = new DownloadServiceBinder();
	private Map<Integer, DownloaderThread> allDownloaders = new HashMap<Integer, DownloaderThread>();
	private MediaItemDAO dao;
	
	private DownloadProgressListener downloadProgressListener;
	
	@Override
	public void onCreate() {
		super.onCreate();
		Log.d(TAG, "lifecycle method: onCreate()");
		dao = ShutterStockDemoApp.getMediaItemDao();
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		Log.d(TAG, "lifecycle method: onDestroy()");
	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		Log.d(TAG, "onStartCommand() called!!");
		if (!hasServiceEverStarted) {
			restartUnfinishedDownloads();
			hasServiceEverStarted = true;
		}
		return START_STICKY;
	}
	
	private void restartUnfinishedDownloads() {
		Log.d(TAG, "restartUnfinishedDownloads()");
		List<MediaItem> unfinishedDownlods = dao.getUnfinishedDownloads();
		for (MediaItem item : unfinishedDownlods) {
			Log.d(TAG, "restartUnfinishedDownloads() : restarting download for : " + item);
			addDownload(item);
		}
	}
	
	private void addDownload(MediaItem mediaItem) {
		DownloaderThread d = new DownloaderThread(this, mediaItem);
		downloadExecutorService.submit(d);
		allDownloaders.put(mediaItem.getId(), d);
	}

	@Override
	public IBinder onBind(Intent intent) {
		return mBinder;
	}
	
	public class DownloadServiceBinder extends Binder {
		public DownloadService getService() {
			return DownloadService.this;
		}
	}
	
	/**
	 * This method gets called from the LibraryFragment to start or stop a download
	 * @param mediaItem
	 */
	public void startOrStopDownloading(MediaItem mediaItem) {
		Log.d(TAG, "startOrStopDownloading() 1.0");
		if (!allDownloaders.containsKey(mediaItem.getId())) {
			addDownload(mediaItem);
		} else {
			DownloaderThread t = allDownloaders.remove(mediaItem.getId());
			t.cancel(true);
		}
	}
	
	/**
	 * The LibraryFragment needs to get download progress updates, so it sets itself as a listener
	 * @param
	 */
	public void setDownloadProgressListener(DownloadProgressListener d) {
		downloadProgressListener = d;
	}

	/**
	 * This method gets called from the DownloaderThread
	 */
	@Override
	public void receiveDownloadStatusUpdateFromThread(MediaItem item) {
		if (downloadProgressListener != null) {
			downloadProgressListener.progressUpdated(item);
		}
	}

	@Override
	public void removeFromAllDownloaders(MediaItem mediaItem) {
		allDownloaders.remove(mediaItem.getId());
	}
}
