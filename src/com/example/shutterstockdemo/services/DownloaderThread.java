package com.example.shutterstockdemo.services;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.StringTokenizer;

import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import com.example.shutterstockdemo.ShutterStockDemoApp;
import com.example.shutterstockdemo.db.MediaItemDAO;
import com.example.shutterstockdemo.model.internal.MediaItem;
import com.example.shutterstockdemo.utils.Utils;

public class DownloaderThread implements Runnable {

	public static String TAG = DownloaderThread.class.getSimpleName();
	
	
	private DownloadServiceCallbacks downloadServiceCallbacks;
	private MediaItemDAO dao;
	private MediaItem mediaItem;
	
	private Boolean isRunning;
	private Boolean cancel = false;
	
	
	/**
	 * @param downloadService
	 * @param mediaItem
	 */
	public DownloaderThread(DownloadServiceCallbacks downloadService, MediaItem mediaItem) {
		this.downloadServiceCallbacks = downloadService;
		this.mediaItem = mediaItem;
		this.dao = ShutterStockDemoApp.getMediaItemDao();
	}
	
	/**
	 * @return
	 */
	public Boolean isRunning() {
		return isRunning;
	}
	
	
	/**
	 * This method allows a currently running download to be cancelled/stopped
	 * @param cancel
	 */
	public void cancel(Boolean cancel) {
		Log.d(TAG, "setting cancel = " + cancel);
		this.cancel = cancel;
	}
	
	
	@Override
	public void run() {
		isRunning = true;
		downloadFile();
		isRunning = false;
		
		if (cancel) {
			dao.updateColumnValueDownloadStatus((Long.valueOf(mediaItem.getId())), MediaItemDAO.DownloadStatusEnum.STOPPED);
		}
		
		downloadServiceCallbacks.removeFromAllDownloaders(mediaItem);
	}
	
	private void downloadFile() {
		
		if (Utils.isExternalStorageAvailabe()) {
			Utils.createDirectories();
		} else {
			Toast.makeText(ShutterStockDemoApp.getAppContext(), "Could not mount external storage!  Try again after external storage becomes available.", Toast.LENGTH_SHORT).show();
			return;
		}
		
		
		OutputStream fileOutputStream = null;
		InputStream inputStream = null;
		try {
			URL url = new URL(mediaItem.getUrl());
			
			StringTokenizer tokenizer = new StringTokenizer(mediaItem.getUrl(), "//");
			Log.d(TAG, "url = " + mediaItem.getUrl());
			String fileToDownload = "";
			if (tokenizer != null) {
				while (tokenizer.hasMoreTokens()) {
					fileToDownload = tokenizer.nextToken();
				}
			}
	
			URLConnection connection = url.openConnection();
			File fileThatPossiblyExists = new File(Environment.getExternalStoragePublicDirectory(mediaItem.getMediaType().getDirectory()), fileToDownload);
			Log.d(TAG, "downloadFile() 1.0 | looking for file at: " + fileThatPossiblyExists.getAbsolutePath());
			if (!fileThatPossiblyExists.exists()) {
				Log.d(TAG, "downloadFile() | file does not exist.  creating it now.");
				fileThatPossiblyExists.createNewFile();
			} else {
				Log.d(TAG, "downloadFile() | file already exists.  it's byte size is : " + fileThatPossiblyExists.length());
			}

			fileOutputStream = new FileOutputStream(fileThatPossiblyExists, true);
			long numBytesDownloadedSoFar = fileThatPossiblyExists.length();
			
			connection.setRequestProperty("Range", "bytes=" + numBytesDownloadedSoFar + "-");
			connection.connect();
	
			int numBytesToDownload = connection.getContentLength();
			Log.d(TAG, "downloadFile() | numBytesToDownload should be: " + numBytesToDownload);
	
			inputStream = connection.getInputStream();
			byte data[] = new byte[1024];
	
			mediaItem.setFileLocation(fileThatPossiblyExists.getAbsolutePath());
			if (mediaItem.getTotalBytes() <= 0) {
				mediaItem.setTotalBytes(numBytesToDownload);
				dao.updateColumnValueTotalBytes(Long.valueOf(mediaItem.getId()), (int)numBytesToDownload);
				dao.updateColumnFileLocation(Long.valueOf(mediaItem.getId()), fileThatPossiblyExists.getAbsolutePath());
			}
			
			int bytesReadFromInput = -1;
			while (Utils.isNetworkUp() && (bytesReadFromInput = inputStream.read(data, 0, data.length)) != -1 && !cancel) {
			    numBytesDownloadedSoFar += bytesReadFromInput;
			    fileOutputStream.write(data, 0 , bytesReadFromInput);

			    dao.updateColumnValueDownloadedBytesAndStatus(Long.valueOf(mediaItem.getId()), (int)numBytesDownloadedSoFar, fileThatPossiblyExists.getAbsolutePath(), MediaItemDAO.DownloadStatusEnum.INCOMPLETE);
			    mediaItem.setDownloadedBytes((int)numBytesDownloadedSoFar);
			    downloadServiceCallbacks.receiveDownloadStatusUpdateFromThread(mediaItem);
			}
		} catch(IOException e) {
			Log.e(TAG, "an error occurred .." + e);
			e.printStackTrace();
		} finally {
			try {if(fileOutputStream != null) fileOutputStream.close();} catch(Exception e) {}
			try {if(inputStream != null) inputStream.close();} catch(Exception e) {}
		}
		
		mediaItem = dao.getMediaItemRow(Long.valueOf(mediaItem.getId()));
		if (mediaItem != null && mediaItem.getDownloadedBytes() >= mediaItem.getTotalBytes()) {
			dao.updateColumnValueDownloadStatus((Long.valueOf(mediaItem.getId())), MediaItemDAO.DownloadStatusEnum.COMPLETE);
		}
		
	}

}
