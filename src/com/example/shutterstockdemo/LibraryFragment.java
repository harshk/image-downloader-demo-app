package com.example.shutterstockdemo;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.shutterstockdemo.db.MediaItemDAO;
import com.example.shutterstockdemo.model.internal.MediaItem;
import com.example.shutterstockdemo.services.DownloadProgressListener;
import com.example.shutterstockdemo.services.DownloadService;
import com.example.shutterstockdemo.services.DownloadService.DownloadServiceBinder;
import com.example.shutterstockdemo.utils.Utils;
import com.squareup.picasso.Picasso;

public class LibraryFragment extends Fragment implements DownloadProgressListener {
    
	public static final String TAG = LibraryFragment.class.getSimpleName();
	
	private ListView listView;
	private LibraryAdapter adapter;
	private List<MediaItem> items;
	private MediaItemDAO dao;
	
	
	private DownloadService mService;
	private boolean boundToService = false;
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		Log.d(TAG, "onCreateView() called!!");
        if (container == null) {
            return null;
        }
        
        LinearLayout layout = (LinearLayout)inflater.inflate(R.layout.tab2, container, false);
        
        listView = (ListView)layout.findViewById(R.id.listview);
        items = new ArrayList<MediaItem>();
        adapter = new LibraryAdapter(getActivity(), R.layout.library_item, items);
        listView.setAdapter(adapter);
        listView.setEmptyView(layout.findViewById(R.id.empty_view));
        listView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				MediaItem m = adapter.getItem(position);
				Intent intent = new Intent();  
				intent.setAction(android.content.Intent.ACTION_VIEW);  
				File file = new File(m.getFileLocation());  
				intent.setDataAndType(Uri.fromFile(file), "image/*");  
				startActivity(intent);
			}
		});
        
        dao = ShutterStockDemoApp.getMediaItemDao();
        
        return layout;
    }
	
	@Override
	public void onStart() {
		super.onStart();
		Log.d(TAG, "onStart() called!");
		
		//bind to the download service
		Intent intent = new Intent(getActivity(), DownloadService.class);
		getActivity().bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
		
		//setup the adapter
		List<MediaItem> mediaItems = dao.getAllMediaItemRows();
		items.clear();
		items.addAll(mediaItems);
		adapter.notifyDataSetChanged();
	}
	
	@Override public void onStop() {
		super.onStop();
		Log.d(TAG, "onStop() called!");
		
		//unbind from the download service
		if (boundToService) {
			getActivity().unbindService(mConnection);
			boundToService = false;
		}
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		Log.d(TAG, "onDestroy() called!");
	}
	
	
	//Connection to the DownloadService
	private ServiceConnection mConnection = new ServiceConnection() {
		
		@Override
		public void onServiceConnected(ComponentName className, IBinder service) {
			DownloadServiceBinder binder = (DownloadServiceBinder) service;
			mService = binder.getService();
			boundToService = true;
			mService.setDownloadProgressListener(LibraryFragment.this);
		}

		@Override
		public void onServiceDisconnected(ComponentName name) {
			boundToService = false;
		}
	};
	
	public class LibraryAdapter extends ArrayAdapter<MediaItem> {

		List<MediaItem> items;
		Context context;
		int layoutResourceId;
		LayoutInflater inflater;
		
		public LibraryAdapter(Context context, int layoutResourceId, List<MediaItem> items) {
			super(context, layoutResourceId, items);
			this.items = items;
			this.context = context;
			this.layoutResourceId = layoutResourceId;
			inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		}
		
		@Override
		  public View getView(int position, View convertView, ViewGroup parent) {
			final MediaItem item = getItem(position);
			int progress = calculateProgress(item);
			
			ViewHolder holder;
			if (convertView == null)  {
	        	convertView = inflater.inflate(layoutResourceId, null);
	            holder = new ViewHolder();
	            
	            holder.artwork = (ImageView)convertView.findViewById(R.id.artwork);
	            holder.mediaType = (TextView)convertView.findViewById(R.id.media_type);
	            holder.name = (TextView)convertView.findViewById(R.id.artist_name);
	            holder.progress = (ProgressBar)convertView.findViewById(R.id.progress_bar);
	            holder.downloadPercentage = (TextView)convertView.findViewById(R.id.download_percent);
	            holder.downloadButton = (Button)convertView.findViewById(R.id.download_btn);
	            
	            convertView.setTag(holder);
	        } else {
	        	holder = (ViewHolder)convertView.getTag();
	        }
			
			Picasso.with(context).load(item.getThumbnailUrl()).into(holder.artwork);
			holder.mediaType.setText(item.getMediaType().name());
			holder.name.setText(item.getArtistName());
			holder.progress.setProgress(progress);
			holder.downloadPercentage.setText(progress + "%");
			holder.downloadButton.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					if (!Utils.isNetworkUp()) {
						Toast.makeText(ShutterStockDemoApp.getAppContext(), "no network connectivity available.  try again later", Toast.LENGTH_SHORT).show();
						return;
					}
					
					if (boundToService) {
						mService.startOrStopDownloading(item);
					}
				}
			});
			
			if (progress >= 100) {
				holder.downloadButton.setVisibility(View.GONE);
			} else {
				holder.downloadButton.setVisibility(View.VISIBLE);
			}
			
			return convertView;
		}
		
		
		private Integer calculateProgress(MediaItem item) {
			if (item.getDownloadedBytes() == 0 || item.getTotalBytes() == 0) {
				return 0;
			}
			Integer currentProgress = (int)(((float)item.getDownloadedBytes() / (float)item.getTotalBytes()) * 100);
			currentProgress = (currentProgress > 100) ? currentProgress = 100 : currentProgress;
			return currentProgress;
		}
		
		private class ViewHolder {
			ImageView artwork;
			TextView mediaType;
			TextView name;
			ProgressBar progress;
			TextView downloadPercentage;
			Button downloadButton;
		}
		
	}
	
	int progressUpdatedCounter = -1;
	@Override
	public void progressUpdated(MediaItem mediaItem) {
		Log.d(TAG, "progressUpdated() called!");
		progressUpdatedCounter++;
		
		//we don't want to call notifyDataSetChanged too often because it degrades responsiveness
		//of the UI, so call it only after every 20th time this method gets called
		if (progressUpdatedCounter % 20 == 0 || (mediaItem.getDownloadedBytes() >= mediaItem.getTotalBytes())) {
			for (int i = 0; i < adapter.getCount(); i++) {
				MediaItem item = adapter.getItem(i);
				if (item.getId().equals(mediaItem.getId())) {
					item.setDownloadedBytes(mediaItem.getDownloadedBytes());
					Log.d(TAG, "progressUpdated()  | calling notifyDataSetChanged()");
					
					getActivity().runOnUiThread(new Runnable() {
						public void run() {
							adapter.notifyDataSetChanged();
					    }
					});
					
					return;	
				}
			}
		}
		
	}
}