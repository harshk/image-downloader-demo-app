package com.example.shutterstockdemo.utils;

import java.io.File;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;

import com.example.shutterstockdemo.ShutterStockDemoApp;

public class Utils {
	
	public static String TAG = Utils.class.getSimpleName();
	
	public enum MediaType {
		PICTURE(Environment.DIRECTORY_PICTURES),
		VIDEO(Environment.DIRECTORY_MOVIES),
		AUDIO(Environment.DIRECTORY_MUSIC);
		
		private String directory;
		
		MediaType(String directory) {
			this.directory = directory;
		}
		
		public String getDirectory() {
			return directory;
		}
	}
	
	
	/**
	 * Checks to see if external storage is available for downloaded files to be written to
	 * @return
	 */
	public static Boolean isExternalStorageAvailabe() {
		String writeableState = Environment.getExternalStorageState();
		if (Environment.MEDIA_MOUNTED.equals(writeableState)) {
		    return true;
		}
		return false;
	}
	
	/**
	 * Creates the directories in which the downloaded files will be stored.
	 */
	public static void createDirectories() {
		MediaType mediaTypes[] = MediaType.values();
		for (MediaType m: mediaTypes) {
			File path = Environment.getExternalStoragePublicDirectory(m.getDirectory());
			if (!path.exists()) {
				path.mkdirs();
			}
		}
	}
	
	/**
	 * Checks to see if any network connection is available (data or cell).
	 * @return
	 */
	public static boolean isNetworkUp() {
		ConnectivityManager connectivityManager = (ConnectivityManager)ShutterStockDemoApp.getAppContext().getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
		return activeNetworkInfo != null && activeNetworkInfo.isConnected();
	}
	
}
