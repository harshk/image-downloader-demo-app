package com.example.shutterstockdemo;

import android.app.Application;
import android.content.Context;
import android.content.Intent;

import com.example.shutterstockdemo.db.MediaItemDAO;
import com.example.shutterstockdemo.services.DownloadService;

public class ShutterStockDemoApp extends Application {

	public static MediaItemDAO mediaItemDao;
	public static Context appContext;
	
	@Override
	public void onCreate() {
		appContext = getApplicationContext();
		mediaItemDao = new MediaItemDAO(getApplicationContext());
		mediaItemDao.open();
		
		//start service
		Intent intent = new Intent(getApplicationContext(), DownloadService.class);
		startService(intent);
	}
	
	public static Context getAppContext() {
		return appContext;
	}
	
	public static MediaItemDAO getMediaItemDao() {
		return mediaItemDao;
	}
	
}
